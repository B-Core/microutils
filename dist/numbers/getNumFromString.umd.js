(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/getNumFromString'] = global['/getNumFromString'] || {}, global['/getNumFromString'].js = factory());
}(this, (function () { 'use strict';

  var getNumFromString = (str) => {
    if (typeof str === 'string') {
      const num = str.match(/-(?=\d)|\d+|\.\d+/g);
      return num !== null
              ? parseFloat(num.join(''))
              : 0;
    }
    return typeof parseFloat(str) === 'number' && !isNaN(parseFloat(str))
          ? parseFloat(str)
          : 0;
  };

  return getNumFromString;

})));
