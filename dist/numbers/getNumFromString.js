var getNumFromString = (str) => {
  if (typeof str === 'string') {
    const num = str.match(/-(?=\d)|\d+|\.\d+/g);
    return num !== null
            ? parseFloat(num.join(''))
            : 0;
  }
  return typeof parseFloat(str) === 'number' && !isNaN(parseFloat(str))
        ? parseFloat(str)
        : 0;
};

export default getNumFromString;
