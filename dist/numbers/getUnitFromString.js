const reg = new RegExp(/%|px|vh|vw|em|deg/, 'g');

var getUnitFromString = (str) => {
  let unit = '';
  if (typeof str === 'string') {
    const u = str.match(reg);
    unit = u !== null
            ? u[0]
            : '';
  }
  return unit;
};

export default getUnitFromString;
