(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/getUnitFromString'] = global['/getUnitFromString'] || {}, global['/getUnitFromString'].js = factory());
}(this, (function () { 'use strict';

  const reg = new RegExp(/%|px|vh|vw|em|deg/, 'g');

  var getUnitFromString = (str) => {
    let unit = '';
    if (typeof str === 'string') {
      const u = str.match(reg);
      unit = u !== null
              ? u[0]
              : '';
    }
    return unit;
  };

  return getUnitFromString;

})));
