(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/clamp'] = global['/clamp'] || {}, global['/clamp'].js = factory());
}(this, (function () { 'use strict';

  var clamp = (n, min, max) => {
    if (typeof n !== 'number') throw new TypeError(`${n} is not a number`);
    return Math.min(Math.max(n, min), max);
  };

  return clamp;

})));
