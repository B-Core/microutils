const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var regexp = ({
  COMMENT_REG_EXP,
  NEW_LINE_REG_EXP,
  BACKTICK_REG_EXP,
  COMMENTS_AND_BACKTICK,
});

export default regexp;
export { COMMENT_REG_EXP, NEW_LINE_REG_EXP, BACKTICK_REG_EXP, COMMENTS_AND_BACKTICK };
