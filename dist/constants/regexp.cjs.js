'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var regexp = ({
  COMMENT_REG_EXP,
  NEW_LINE_REG_EXP,
  BACKTICK_REG_EXP,
  COMMENTS_AND_BACKTICK,
});

exports.COMMENT_REG_EXP = COMMENT_REG_EXP;
exports.NEW_LINE_REG_EXP = NEW_LINE_REG_EXP;
exports.BACKTICK_REG_EXP = BACKTICK_REG_EXP;
exports.COMMENTS_AND_BACKTICK = COMMENTS_AND_BACKTICK;
exports.default = regexp;
