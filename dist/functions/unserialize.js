const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var unserialize = (serialized) => {
  const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP, '').replace(BACKTICK_REG_EXP, ''));
  const { body } = serialized;
  let func = new Function();
  try {
    func = new Function(args, body);
  } catch (e) {
    console.error(e);
  }
  return func;
};

export default unserialize;
