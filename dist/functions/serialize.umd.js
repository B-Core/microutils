(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/serialize'] = global['/serialize'] || {}, global['/serialize'].js = factory());
}(this, (function () { 'use strict';

  const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
  const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
  const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');

  const serialize = (funct) => {
    if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
    const txt = funct.toString();
    const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
    const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
    return {
      args: args.map(el => el.replace(COMMENT_REG_EXP, '')
        .replace(NEW_LINE_REG_EXP, '')
        .replace(BACKTICK_REG_EXP, '')),
      body,
    };
  };

  return serialize;

})));
