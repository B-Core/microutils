var isArray = val => val instanceof Array;

var groupBy = (array, iteratee) => {
  if (!isArray(array)) throw new TypeError(`${array} must be an array`);
  if (typeof iteratee === 'undefined') throw new Error('iteratee must be defined, else it is just the base array');
  return array.reduce((r, v, i, a, k = v[iteratee]) => ((r[k] || (r[k] = [])).push(v), r), {});
};

export default groupBy;
