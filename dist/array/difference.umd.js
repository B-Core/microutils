(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/difference'] = global['/difference'] || {}, global['/difference'].js = factory());
}(this, (function () { 'use strict';

  var difference = (source, inputs) => {
    const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
    let diffs = source
    .filter(i => (i !== null && typeof i !== 'undefined'))
    .filter((i) => {
      const e = i instanceof Object ? JSON.stringify(i) : i;
      const index = flatArray.indexOf(e);
      if (index >= 0) flatArray.splice(index, 1);
      return !(index >= 0);
    });
    return diffs;
  };

  return difference;

})));
