'use strict';

function findIndex(array, predicate) {
  let index = -1;

  for (let i = 0;i < array.length;i++) {
    index = i;
    if (predicate(array[index])) break;
    else index = -1;
  }

  return index;
}

module.exports = findIndex;
