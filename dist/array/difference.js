var difference = (source, inputs) => {
  const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
  let diffs = source
  .filter(i => (i !== null && typeof i !== 'undefined'))
  .filter((i) => {
    const e = i instanceof Object ? JSON.stringify(i) : i;
    const index = flatArray.indexOf(e);
    if (index >= 0) flatArray.splice(index, 1);
    return !(index >= 0);
  });
  return diffs;
};

export default difference;
