(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/findIndex'] = global['/findIndex'] || {}, global['/findIndex'].js = factory());
}(this, (function () { 'use strict';

  function findIndex(array, predicate) {
    let index = -1;

    for (let i = 0;i < array.length;i++) {
      index = i;
      if (predicate(array[index])) break;
      else index = -1;
    }

    return index;
  }

  return findIndex;

})));
