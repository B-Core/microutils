(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/closest'] = global['/closest'] || {}, global['/closest'].js = factory());
}(this, (function () { 'use strict';

  var closest = (array, num) => {
    let i = 0;
    let minDiff = 1e16;
    let ans;
    for (i in array) {
      const m = Math.abs(num - array[i]);
      if (m < minDiff) {
        minDiff = m;
        ans = array[i];
      }
    }
    return ans;
  };

  return closest;

})));
