this['/index'] = this['/index'] || {};
this['/index'].js = (function () {
  'use strict';

  var closest = (array, num) => {
    let i = 0;
    let minDiff = 1e16;
    let ans;
    for (i in array) {
      const m = Math.abs(num - array[i]);
      if (m < minDiff) {
        minDiff = m;
        ans = array[i];
      }
    }
    return ans;
  };

  var difference = (source, inputs) => {
    const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
    let diffs = source
    .filter(i => (i !== null && typeof i !== 'undefined'))
    .filter((i) => {
      const e = i instanceof Object ? JSON.stringify(i) : i;
      const index = flatArray.indexOf(e);
      if (index >= 0) flatArray.splice(index, 1);
      return !(index >= 0);
    });
    return diffs;
  };

  function findIndex(array, predicate) {
    let index = -1;

    for (let i = 0;i < array.length;i++) {
      index = i;
      if (predicate(array[index])) break;
      else index = -1;
    }

    return index;
  }

  var isArray = val => val instanceof Array;

  var groupBy = (array, iteratee) => {
    if (!isArray(array)) throw new TypeError(`${array} must be an array`);
    if (typeof iteratee === 'undefined') throw new Error('iteratee must be defined, else it is just the base array');
    return array.reduce((r, v, i, a, k = v[iteratee]) => ((r[k] || (r[k] = [])).push(v), r), {});
  };

  const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
  const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
  const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
  const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

  var regexp = ({
    COMMENT_REG_EXP,
    NEW_LINE_REG_EXP,
    BACKTICK_REG_EXP,
    COMMENTS_AND_BACKTICK,
  });

  const serialize = (funct) => {
    if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
    const txt = funct.toString();
    const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
    const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
    return {
      args: args.map(el => el.replace(COMMENT_REG_EXP, '')
        .replace(NEW_LINE_REG_EXP, '')
        .replace(BACKTICK_REG_EXP, '')),
      body,
    };
  };

  var unserialize = (serialized) => {
    const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP, '').replace(BACKTICK_REG_EXP, ''));
    const { body } = serialized;
    let func = new Function();
    try {
      func = new Function(args, body);
    } catch (e) {
      console.error(e);
    }
    return func;
  };

  /**
   * Deep copy the given object with JSON.parse and stringify.
   *
   * @param {*} obj
   * @param {Array<Object>} cache
   * @return {*}
   */
  function deepCopy(obj) {
    // just return if obj is immutable value
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }

    const copy = JSON.parse(JSON.stringify(obj));

    return copy;
  }

  /**
   * getProp utility - an alternative to lodash.get
   * @author @harish2704, @muffypl, @pi0
   * @param {Object} object
   * @param {String|Array} path
   * @param {*} defaultVal
   */
  const getProp = function getProp(object, path, defaultVal) {
    const _path = Array.isArray(path) ?
      path :
      path.split('.').filter(i => i.length);

    if (!_path.length) {
      return object === undefined ? defaultVal : object
    }

    return getProp(object[_path.shift()], _path, defaultVal)
  };

  var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

  const merge = (obj, ...src) => {
    let errorMessage = 'only arrays and objects are mergeable';
    const sourceCheck = src.reduce((acc, s, i) => {
      if (acc === false) return acc;
      if ((isObject(obj) && !isObject(s)) || (isArray(obj) && !isArray(s))) {
        acc = false;
        errorMessage = `obj and source[${i}] must be of same type`;
        return acc;
      }
      acc &= !(isObject(s) || isArray(s));
      return Boolean(src.length > 1 ? acc : !acc);
    }, true);

    if (!sourceCheck || !(isObject(obj) || isArray(obj))) {
      throw new TypeError(errorMessage);
    }
    const newObj = src.reduce((acc, value) => { 
      if (typeof value !== 'undefined' && value !== null && isObject(value)) {
        const keys = Object.keys(value);
        keys.forEach((k) => {
          if (typeof value[k] !== 'undefined' && value[k] !== null) {
            if ((isObject(value[k]) && isObject(acc[k])) || (isArray(acc[k]) && isArray(value[k]))) {
              acc[k] = merge(acc[k], value[k]);
            } else {
              acc[k] = value[k];
            }
          } 
        });
      } else if (typeof value !== 'undefined' && value !== null && isArray(value)) {
        acc.forEach((_, i) => {
          const v = value.shift();
          if (typeof v !== 'undefined' && v !== null) acc[i] = ((isObject(acc[i]) && isObject(v)) || isArray(acc[i]) && isArray(v)) ? merge(acc[i], v) : v;
        });
        acc = acc.concat(value);
      }
      return acc;
    }, deepCopy(obj));

    return newObj;
  };

  var parseBooleans = (string) => {
    if (string === '') {
      return true;
    }
    if (typeof string === 'undefined' || string === 'false' || Boolean(string) === false) {
      return false;
    }
    return Boolean(string);
  };

  const setProp = function setProp(object, keys, val){
    keys = Array.isArray( keys )? keys : keys.split('.');
    if( keys.length > 1 ){
      object[keys[0]] = object[keys[0]] || {};
      return setProp( object[keys[0]], keys.slice(1), val );
    }
    object[keys[0]] = val;
  };

  var uuid = () => {
    let d = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now(); // use high-precision timer if available
    }
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x'
              ? r
              : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  };

  var clamp = (n, min, max) => {
    if (typeof n !== 'number') throw new TypeError(`${n} is not a number`);
    return Math.min(Math.max(n, min), max);
  };

  var getNumFromString = (str) => {
    if (typeof str === 'string') {
      const num = str.match(/-(?=\d)|\d+|\.\d+/g);
      return num !== null
              ? parseFloat(num.join(''))
              : 0;
    }
    return typeof parseFloat(str) === 'number' && !isNaN(parseFloat(str))
          ? parseFloat(str)
          : 0;
  };

  const reg = new RegExp(/%|px|vh|vw|em|deg/, 'g');

  var getUnitFromString = (str) => {
    let unit = '';
    if (typeof str === 'string') {
      const u = str.match(reg);
      unit = u !== null
              ? u[0]
              : '';
    }
    return unit;
  };

  const modules = {
    closest,
    difference,
    findIndex,
    groupBy,
    regexp,
    serialize,
    unserialize,
    deepCopy,
    get: getProp,
    isArray,
    isObject,
    merge,
    parseBooleans,
    set: setProp,
    uuid,
    clamp,
    getNumFromString,
    getUnitFromString,
  };

  return modules;

}());
