(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/merge'] = global['/merge'] || {}, global['/merge'].js = factory());
}(this, (function () { 'use strict';

  /**
   * Deep copy the given object with JSON.parse and stringify.
   *
   * @param {*} obj
   * @param {Array<Object>} cache
   * @return {*}
   */
  function deepCopy(obj) {
    // just return if obj is immutable value
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }

    const copy = JSON.parse(JSON.stringify(obj));

    return copy;
  }

  var isArray = val => val instanceof Array;

  var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

  const merge = (obj, ...src) => {
    let errorMessage = 'only arrays and objects are mergeable';
    const sourceCheck = src.reduce((acc, s, i) => {
      if (acc === false) return acc;
      if ((isObject(obj) && !isObject(s)) || (isArray(obj) && !isArray(s))) {
        acc = false;
        errorMessage = `obj and source[${i}] must be of same type`;
        return acc;
      }
      acc &= !(isObject(s) || isArray(s));
      return Boolean(src.length > 1 ? acc : !acc);
    }, true);

    if (!sourceCheck || !(isObject(obj) || isArray(obj))) {
      throw new TypeError(errorMessage);
    }
    const newObj = src.reduce((acc, value) => { 
      if (typeof value !== 'undefined' && value !== null && isObject(value)) {
        const keys = Object.keys(value);
        keys.forEach((k) => {
          if (typeof value[k] !== 'undefined' && value[k] !== null) {
            if ((isObject(value[k]) && isObject(acc[k])) || (isArray(acc[k]) && isArray(value[k]))) {
              acc[k] = merge(acc[k], value[k]);
            } else {
              acc[k] = value[k];
            }
          } 
        });
      } else if (typeof value !== 'undefined' && value !== null && isArray(value)) {
        acc.forEach((_, i) => {
          const v = value.shift();
          if (typeof v !== 'undefined' && v !== null) acc[i] = ((isObject(acc[i]) && isObject(v)) || isArray(acc[i]) && isArray(v)) ? merge(acc[i], v) : v;
        });
        acc = acc.concat(value);
      }
      return acc;
    }, deepCopy(obj));

    return newObj;
  };

  return merge;

})));
