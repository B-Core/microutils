'use strict';

var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

module.exports = isObject;
