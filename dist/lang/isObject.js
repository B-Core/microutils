var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

export default isObject;
