(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global['/parseBooleans'] = global['/parseBooleans'] || {}, global['/parseBooleans'].js = factory());
}(this, (function () { 'use strict';

  var parseBooleans = (string) => {
    if (string === '') {
      return true;
    }
    if (typeof string === 'undefined' || string === 'false' || Boolean(string) === false) {
      return false;
    }
    return Boolean(string);
  };

  return parseBooleans;

})));
