this['/parseBooleans'] = this['/parseBooleans'] || {};
this['/parseBooleans'].js = (function () {
  'use strict';

  var parseBooleans = (string) => {
    if (string === '') {
      return true;
    }
    if (typeof string === 'undefined' || string === 'false' || Boolean(string) === false) {
      return false;
    }
    return Boolean(string);
  };

  return parseBooleans;

}());
