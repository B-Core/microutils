this['/deepCopy'] = this['/deepCopy'] || {};
this['/deepCopy'].js = (function () {
  'use strict';

  /**
   * Deep copy the given object with JSON.parse and stringify.
   *
   * @param {*} obj
   * @param {Array<Object>} cache
   * @return {*}
   */
  function deepCopy(obj) {
    // just return if obj is immutable value
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }

    const copy = JSON.parse(JSON.stringify(obj));

    return copy;
  }

  return deepCopy;

}());
