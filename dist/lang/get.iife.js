this['/get'] = this['/get'] || {};
this['/get'].js = (function () {
  'use strict';

  /**
   * getProp utility - an alternative to lodash.get
   * @author @harish2704, @muffypl, @pi0
   * @param {Object} object
   * @param {String|Array} path
   * @param {*} defaultVal
   */
  const getProp = function getProp(object, path, defaultVal) {
    const _path = Array.isArray(path) ?
      path :
      path.split('.').filter(i => i.length);

    if (!_path.length) {
      return object === undefined ? defaultVal : object
    }

    return getProp(object[_path.shift()], _path, defaultVal)
  };

  return getProp;

}());
