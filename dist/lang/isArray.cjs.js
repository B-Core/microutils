'use strict';

var isArray = val => val instanceof Array;

module.exports = isArray;
