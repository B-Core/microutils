(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['/isArray'] = global['/isArray'] || {}, global['/isArray'].js = factory());
}(this, (function () { 'use strict';

	var isArray = val => val instanceof Array;

	return isArray;

})));
