export const testObject1 = {
    test: {
        name: 'test',
        age: 10,
        metadata: {
            data1: [1, 2, 3],
            data2: {
                id: 'id-test-1',
            },
        },
    }, 
    testArray: ['test', 10, {
         test: {
            name: 'test',
            age: 10,
            metadata: {
                data1: [1, 2, 3],
                data2: {
                    id: 'id-test-1',
                },
            },
        },   
    }],
};

export const testObject2 = {
    test: {
        metadata: {
            data1: [1, 2, 3, 5, 6, 7],
            data2: {
                id: 'id-test-2',
                name: 'test',
            },
        },
        date: '11-03-2019',
    },
    testArray: [{
        isItHere: true,
    }, 'test', {
         test: {
            name: 'test2',
            age: 18,
            metadata: {
                data1: [3, 4, 5],
            },
        },   
    }],
};

export const testObjectResult = {
    test: {
        name: 'test',
        age: 10,
        metadata: {
            data1: [1, 2, 3, 5, 6, 7],
            data2: {
                id: 'id-test-2',
                name: 'test',
            },
        },
        date: '11-03-2019',
    }, 
    testArray: [{
        isItHere: true,
    }, 'test', {
         test: {
            name: 'test2',
            age: 18,
            metadata: {
                data1: [3, 4, 5],
                data2: {
                    id: 'id-test-1',
                },
            },
        },   
    }],
};
