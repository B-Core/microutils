export default val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));
