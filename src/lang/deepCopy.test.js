import deepCopy from './deepCopy';

describe('deepCopy: ', () => { 
  test('should return the object if it is immutable', () => {
    const values = [
      'test',
      10,
      10.5,
      null,
      undefined,
    ];

    values.forEach(val => expect(deepCopy(val)).toEqual(val));
  });
  
  test('should return the object if it is in circular structure', () => {
    const circular = document.createElement('div');
    const cp = deepCopy(circular);

    expect(deepCopy(circular)).not.toEqual(circular);
    expect(deepCopy(cp)).toEqual(cp);
  });

  test('should copy an object entirely', () => {
    const mock = jest.fn().mockReturnValue('test');
    const obj = {
      test: 'test',
      lol: 10,
      fn: mock,
      ar: [1, 2, 3, {
        test: 'test',
      }],
    };
    const res = {
      test: 'test',
      lol: 10,
      ar: [1, 2, 3, {
        test: 'test',
      }],
    };

    const cp = deepCopy(obj);

    expect(cp).toEqual(res);
    expect(() => cp.fn()).toThrow('fn is not a function');
  });
});
