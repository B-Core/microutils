const setProp = function setProp(object, keys, val){
  keys = Array.isArray( keys )? keys : keys.split('.');
  if( keys.length > 1 ){
    object[keys[0]] = object[keys[0]] || {};
    return setProp( object[keys[0]], keys.slice(1), val );
  }
  object[keys[0]] = val;
};

export default setProp;