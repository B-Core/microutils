import deepCopy from './deepCopy';
import isArray from './isArray';
import isObject from './isObject';


const merge = (obj, ...src) => {
  let errorMessage = 'only arrays and objects are mergeable';
  const sourceCheck = src.reduce((acc, s, i) => {
    if (acc === false) return acc;
    if ((isObject(obj) && !isObject(s)) || (isArray(obj) && !isArray(s))) {
      acc = false;
      errorMessage = `obj and source[${i}] must be of same type`;
      return acc;
    }
    acc &= !(isObject(s) || isArray(s));
    return Boolean(src.length > 1 ? acc : !acc);
  }, true);

  if (!sourceCheck || !(isObject(obj) || isArray(obj))) {
    throw new TypeError(errorMessage);
  }
  const newObj = src.reduce((acc, value) => { 
    if (typeof value !== 'undefined' && value !== null && isObject(value)) {
      const keys = Object.keys(value);
      keys.forEach((k) => {
        if (typeof value[k] !== 'undefined' && value[k] !== null) {
          if ((isObject(value[k]) && isObject(acc[k])) || (isArray(acc[k]) && isArray(value[k]))) {
            acc[k] = merge(acc[k], value[k]);
          } else {
            acc[k] = value[k];
          }
        } 
      });
    } else if (typeof value !== 'undefined' && value !== null && isArray(value)) {
      acc.forEach((_, i) => {
        const v = value.shift();
        if (typeof v !== 'undefined' && v !== null) acc[i] = ((isObject(acc[i]) && isObject(v)) || isArray(acc[i]) && isArray(v)) ? merge(acc[i], v) : v;
      });
      acc = acc.concat(value);
    }
    return acc;
  }, deepCopy(obj));

  return newObj;
};

export default merge;
