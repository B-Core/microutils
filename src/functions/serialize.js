import { COMMENT_REG_EXP, NEW_LINE_REG_EXP, BACKTICK_REG_EXP } from '../constants/regexp';

const serialize = (funct) => {
  if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
  const txt = funct.toString();
  const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
  const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
  return {
    args: args.map(el => el.replace(COMMENT_REG_EXP, '')
      .replace(NEW_LINE_REG_EXP, '')
      .replace(BACKTICK_REG_EXP, '')),
    body,
  };
};

export default serialize;
