const reg = new RegExp(/%|px|vh|vw|em|deg/, 'g')

export default (str) => {
  let unit = '';
  if (typeof str === 'string') {
    const u = str.match(reg);
    unit = u !== null
            ? u[0]
            : '';
  }
  return unit;
};