module.exports = {
  performance: {
    hints: "warning", // enum
    maxAssetSize: 20000, // int (in bytes),
    maxEntrypointSize: 10000, // int (in bytes)
    assetFilter: function(assetFilename) {
      // Function predicate that provides asset filenames
      return assetFilename.endsWith('.js');
    }
  },
  optimization: {
    splitChunks: {
      chunks: "all",
      minSize: 10000,
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        type: 'javascript/esm',
        exclude: /node_modules/
      }
    ]
  },
  devtool: "source-map",
};