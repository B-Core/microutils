const webpack = require('webpack');
const path = require('path');

const glob_entries = require('webpack-glob-entries');

const defaultConfig = require('./webpack.default.config');

const array = glob_entries(path.resolve('src/array/*.js'));
const functions = glob_entries(path.resolve('src/functions/*.js'));
const numbers = glob_entries(path.resolve('src/numbers/*.js'));
const lang = glob_entries(path.resolve('src/lang/*.js'));

const entries = {
  index: path.resolve('src/main.js'),
  array,
  functions,
  numbers,
  lang,
};


for (const key in entries) {
  if (key.includes('test')) delete entries[key];
  if (typeof entries[key] === 'object') {
    for (const key2 in entries[key]) {
      if (key2.includes('test')) delete entries[key][key2];
    };
  }
};

const baseConfig = {
  ...defaultConfig,
  mode: process.env.NODE_ENV || "production",
  entry: entries.index,
  plugins: [
    new webpack.ProvidePlugin({
      window: 'window',
    })
  ],
  output: {
    path: path.resolve('dist'),
    filename: '[name].js',
  },
};

const arrayConfig = {
  ...baseConfig,
  entry: entries.array,
  output: {
    path: path.resolve('dist/array'),
    filename: '[name].js',
  },
};

const langConfig = {
  ...baseConfig,
  entry: entries.lang,
  output: {
    path: path.resolve('dist/lang'),
    filename: '[name].js',
  },
};

const functionsConfig = {
  ...baseConfig,
  entry: entries.functions,
  output: {
    path: path.resolve('dist/functions'),
    filename: '[name].js',
  },
};

const numbersConfig = {
  ...baseConfig,
  entry: entries.numbers,
  output: {
    path: path.resolve('dist/numbers'),
    filename: '[name].js',
  },
};

const baseConfigs = [
  baseConfig,
  arrayConfig,
  langConfig,
  functionsConfig,
  numbersConfig,
];

const amdConfigs = baseConfigs.map((c) => ({
  ...c,
  output: {
    ...c.output,
    filename: '[name].amd.js',
    libraryTarget: 'amd',
  }
}));

const commonConfigs = baseConfigs.map((c) => ({
  ...c,
  output: {
    ...c.output,
    filename: '[name].commonjs.js',
    libraryTarget: 'commonjs',
  }
}));

const configs = [
  ...baseConfigs,
  ...amdConfigs,
  ...commonConfigs,
];

module.exports = configs;
